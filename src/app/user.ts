export class User {
    name: string;
    email:string;
    mobile:number;

    constructor(values: Object = {})
    {
        Object.assign(this,values);
    }
}
