import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  angForm: FormGroup;
  submitted = false;
  constructor(private fb: FormBuilder,
    private httpService: HttpClient) {
    this.createForm();
  }
  createForm() {

    this.angForm = this.fb.group({
      name: ['', [Validators.required, Validators.pattern('[a-zA-Z ]*')]],
      email: ['', [Validators.required, Validators.email]],
      mobile: ['', [Validators.required, Validators.pattern('[1-9]{1}[0-9]{9}')] ]
    });
  }
  

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.angForm.invalid) {
      return;
    }
  }


  ngOnInit() {
    // this.angForm = this.fb.group({
    //   name: '',
    //   email: '',
    //   mobile: ''
    // });
  }

  postProfile() {
    let formObj = this.angForm.getRawValue();

    let body = JSON.stringify(formObj);
    alert(body);
    console.log(body);
    const headers = new HttpHeaders();
    headers.set('Content-Type', 'application/json; charset=utf-8');
    this.httpService.post('http://localhost:3000/posts', body, { headers: headers })
      .subscribe(
        (data) => {
          console.log(data);
        },
        (err: HttpErrorResponse) => {
          if (err.error instanceof Error) {
            console.log('Client-side error occured.');
          } else {
            console.log('Server-side error occured.');
          }
        }
      );
  }

}
