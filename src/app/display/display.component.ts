import { Component, OnInit, Input } from '@angular/core';
import { User } from '../user';
import { HttpClient } from '@angular/common/http';
import { HttpErrorResponse } from '@angular/common/http';


@Component({
  selector: 'app-display',
  templateUrl: './display.component.html',
  styleUrls: ['./display.component.css']
})
export class DisplayComponent implements OnInit {
  empl: String[];

  constructor(private httpService: HttpClient) { }

  ngOnInit() {
    alert("hii");
    this.httpService.get('http://localhost:3000/posts').subscribe(
      data => {
        this.empl = data as string[];
      },
      (err: HttpErrorResponse) => {
        console.log(err.message);
      }
    );
  }

}
